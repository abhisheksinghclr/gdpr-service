package com.cleartrip.gdpr.controller;

import com.cleartrip.gdpr.model.ComplainceRecord;
import com.cleartrip.gdpr.service.UserSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

import static com.cleartrip.gdpr.util.GdprUtil.createCookieMap;

@RestController
@RequestMapping("/user")
public class GdprController {

    @Autowired
    UserSerivce userSerivce;

    @GetMapping
    public ResponseEntity<Boolean> isCompliant(HttpServletRequest request) {
        Map<String, String> cookieMap = createCookieMap(request);
        if(cookieMap.size()==0) return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(userSerivce.verification(cookieMap), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ComplainceRecord> saveUser(HttpServletRequest request) {
        Map<String, String> cookieMap = createCookieMap(request);
        if(cookieMap.size()==0) return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        Optional<ComplainceRecord> complainceRecord = userSerivce.saveComplainceRecord(cookieMap, true);
        return complainceRecord.isPresent() ? ResponseEntity.ok(complainceRecord.get()) :
                new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
