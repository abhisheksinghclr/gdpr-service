package com.cleartrip.gdpr.service;

import com.cleartrip.gdpr.model.ComplainceRecord;

import java.util.Map;
import java.util.Optional;

public interface UserSerivce {

    Boolean verification(Map<String, String> cookieMap);

    Optional<ComplainceRecord> saveComplainceRecord(Map<String, String> userId, boolean isCompliant);
}
