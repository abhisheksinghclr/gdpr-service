package com.cleartrip.gdpr.service.impl;

import com.cleartrip.gdpr.model.ComplainceRecord;
import com.cleartrip.gdpr.repository.UserRepository;
import com.cleartrip.gdpr.service.UserSerivce;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.cleartrip.gdpr.util.Constants.APACHE;
import static com.cleartrip.gdpr.util.Constants.USERID;

@Service
@Slf4j
public class UserServiceImpl implements UserSerivce {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RedisTemplate<String, ComplainceRecord> redisTemplate;

    @Override
    public Boolean verification(Map<String, String> cookieMap) {
        try {

            String id = getApacheOrUserId(cookieMap);

            Optional<ComplainceRecord> complianceRecord = Optional
                    .ofNullable(redisTemplate.opsForValue().get(id));
            if (!complianceRecord.isPresent()) {
                complianceRecord = userRepository.findByUniqueId(id);
                if (complianceRecord.isPresent()) {
                    redisTemplate.opsForValue().set(id, complianceRecord.get());
                } else {
                    ComplainceRecord falseCacheRecord = newComplainceRecord(id, false);
                    redisTemplate.opsForValue().set(id, falseCacheRecord, 7, TimeUnit.DAYS);
                }
            }
            return complianceRecord.isPresent() && complianceRecord.get().isCompliant();
        } catch (Exception ex) {
            log.error("verfication step error: ", ex);
            return false;
        }
    }

    @Override
    public Optional<ComplainceRecord> saveComplainceRecord(Map<String, String> cookieMap, boolean isCompliant) {
        try {
            String id = getApacheOrUserId(cookieMap);
            ComplainceRecord complainceRecord = newComplainceRecord(id, isCompliant);
            complainceRecord = userRepository.save(complainceRecord);
            redisTemplate.opsForValue().set(id, complainceRecord);
            return Optional.ofNullable(complainceRecord);
        } catch (Exception ex) {
            log.error("save step error : ", ex);
            return Optional.empty();
        }
    }

    private ComplainceRecord newComplainceRecord(String id, boolean b) {
        ComplainceRecord falseCacheRecord = new ComplainceRecord();
        falseCacheRecord.setUniqueId(id);
        falseCacheRecord.setCompliant(b);
        return falseCacheRecord;
    }

    private String getApacheOrUserId(Map<String, String> cookieMap) throws UnsupportedEncodingException {
        String value =  cookieMap.containsKey(USERID)?
                cookieMap.get(USERID) : cookieMap.get(APACHE);
        value = URLDecoder.decode(value, StandardCharsets.UTF_8.name());
        String [] values =  value.split("\\|");
        return values[values.length-1];
    }
}
