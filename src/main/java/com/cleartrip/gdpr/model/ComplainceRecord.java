package com.cleartrip.gdpr.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Data
@Document(collection = "compliance")
public class ComplainceRecord {
    @Id
    private BigInteger id;

    @Indexed(unique=true)
    private String uniqueId;

    private boolean isCompliant;

}
