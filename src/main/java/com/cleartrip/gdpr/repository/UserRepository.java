package com.cleartrip.gdpr.repository;

import com.cleartrip.gdpr.model.ComplainceRecord;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface UserRepository extends MongoRepository<ComplainceRecord, BigInteger> {

    Optional<ComplainceRecord> findByUniqueId(String uniqueId);

}
