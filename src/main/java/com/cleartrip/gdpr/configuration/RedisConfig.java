package com.cleartrip.gdpr.configuration;

import com.cleartrip.gdpr.model.ComplainceRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {

    @Value("${ct.gdpr.redis.hostname}")
    private String hostName;

    @Value("${ct.gdpr.redis.port}")
    private int port;

    @Value("${ct.gdpr.redis.max-total}")
    private int maxTotal;

    @Value("${ct.gdpr.redis.max-idle}")
    private int maxIdle;

    @Value("${ct.gdpr.redis.min-idle}")
    private int minIdle;

    @Bean
    JedisConnectionFactory getJedisConnectionFactory(){

        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration(hostName, port);

        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(maxTotal);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setMinIdle(minIdle);

        JedisClientConfiguration clientConfiguration = JedisClientConfiguration.builder()
                .usePooling()
                .poolConfig(poolConfig).build();
        JedisConnectionFactory factory = new JedisConnectionFactory(configuration, clientConfiguration);
        return factory;
    }


    @Bean
    @Primary
    RedisTemplate<String, ComplainceRecord> redisTemplate(){
        RedisTemplate<String, ComplainceRecord> template = new RedisTemplate<>();
        template.setConnectionFactory(getJedisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new Jackson2JsonRedisSerializer(ComplainceRecord.class));
        return template;
    }

}
