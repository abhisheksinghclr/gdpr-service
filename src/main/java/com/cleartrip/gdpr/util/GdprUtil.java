package com.cleartrip.gdpr.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.cleartrip.gdpr.util.Constants.APACHE;
import static com.cleartrip.gdpr.util.Constants.USERID;

public class GdprUtil {

    public static boolean cookiefilter(Cookie cookie1) {
        return cookie1.getName().equals(APACHE) || cookie1.getName().equals(USERID);
    }

    public static Map<String, String> createCookieMap(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        Map<String ,String> cookieMap = new HashMap<>();
        Arrays.asList(cookies).stream()
                .filter(GdprUtil::cookiefilter)
                .forEach(cookie1 -> cookieMap.put(cookie1.getName(), cookie1.getValue()));
        return cookieMap;
    }


}
